# Unpublished Node Permissions

Creates permisisons per node content type to control access to unpublished nodes per content type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/unpublished_node_permissions).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/unpublished_node_permissions).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Go to _/admin/modules_ and enable **Unpublished Node Permissions** module.
- Go to "/admin/people/permissions/module/unpublished_node_permissions" and enable permissions.


## Maintainers

 - Wietze Eilander - [Islanderweedy](https://www.drupal.org/u/islanderweedy)
 - Frans van der Meer - [Frans](https://www.drupal.org/u/frans)
 - Fabien Gutknecht - [Fabsgugu](https://www.drupal.org/u/fabsgugu)
 - Jeroen Bobbeldijk - [jeroen.b](https://www.drupal.org/u/jeroenb)
 - Matthias Glastra - [matglas86](https://www.drupal.org/u/matglas86)
 - Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)

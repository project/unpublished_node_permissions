<?php

namespace Drupal\Tests\unpublished_node_permissions\Functional;

use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Class UnpublishedNodePermissionsTest. The base class for testing permissions.
 */
class UnpublishedNodePermissionsTest extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'unpublished_node_permissions'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test unpublished permissions.
   */
  public function testUnpublishedPermissions() {
    // Log in as an admin user with permission to manage node settings.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create a new node type.
    $this->createContentType(['type' => 'unpublished']);

    // Check the status of the page.
    $this->drupalGet('node/add/unpublished');
    $this->assertSession()->statusCodeEquals(200);

    // Create a new node with a random name.
    $edit = [];
    $edit['title[0][value]'] = $this->randomMachineName(8);
    $this->drupalGet('node/add/unpublished');
    $this->submitForm($edit, 'Save');

    // Get created node.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);

    // Force unpublish the node.
    $node->setUnpublished();
    $node->save();

    // Check the status of the page for the admin.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->statusCodeEquals(200);

    // Log in as a regular user with unpublished access (global permission).
    $user = $this->drupalCreateUser(['access content', 'view unpublished content']);
    $this->drupalLogin($user);

    // Check the status of the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->statusCodeEquals(200);

    // Log in as a regular user with unpublished access (node type permission).
    $user = $this->drupalCreateUser(['access content', 'view unpublished unpublished content']);
    $this->drupalLogin($user);

    // Check the status of the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->statusCodeEquals(200);

    // Log in as a regular user without unpublished access.
    $user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($user);

    // Check the status of the page.
    $this->drupalGet("node/{$node->id()}");
    $this->assertSession()->statusCodeEquals(403);
  }

}
